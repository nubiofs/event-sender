import com.fiftyonred.mock_jedis.MockJedis;
import com.github.fridujo.rabbitmq.mock.MockConnectionFactory;
import com.github.kristofa.test.http.Method;
import com.github.kristofa.test.http.MockHttpServer;
import com.github.kristofa.test.http.SimpleHttpResponseProvider;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import junit.framework.TestCase;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class SenderTest extends TestCase {



    public void testConsume() throws IOException, TimeoutException {
        ConnectionFactory factory = new MockConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("EXCHANGE", "topic");
        Jedis j = new MockJedis("REDIS_HOST");
        //Sender sender = new Sender(channel,j,"EXCHANGE");
        j.set("12345:AvroSchema",positionshcemastring());

        String event = generatePositionEvent(1.0,1.0,1).toString();

        byte [] evnt = AvroSerialize(generatePositionEvent(1.0,1.0,1));


        int PORT = 51234;
        String baseUrl = "http://localhost:" + PORT;
        j.sadd("12345:WebHooks", baseUrl);

        MockHttpServer server;
        SimpleHttpResponseProvider responseProvider;

        responseProvider = new SimpleHttpResponseProvider();
        server = new MockHttpServer(PORT, responseProvider);
        server.start();




        responseProvider.expect(Method.POST, "/", "JSON", event).respondWith(200, "text/plain", "OK");

        channel.basicPublish("EXCHANGE","12345",null,evnt);

        //test if json sent on POST was actually the event generated


        server.stop();

    }


    private static GenericData.Record generatePositionEvent(Double lat, Double lon, Integer carID){
        GenericData.Record event;
        event = new GenericData.Record(new Schema.Parser().parse(positionshcemastring()));
        event.put("carId",carID);
        event.put("latitude",lat);
        event.put("longitude",lon);
        return event;
    }


    private static String positionshcemastring(){
        return "{" +
                "  \"type\" : \"record\"," +
                "  \"name\" : \"Position\"," +
                "  \"fields\" : [ {" +
                "    \"name\" : \"carId\"," +
                "    \"type\" : \"int\"" +
                "  }, {" +
                "    \"name\" : \"latitude\"," +
                "    \"type\" : \"double\"" +
                "  }, {" +
                "    \"name\" : \"longitude\"," +
                "    \"type\" : \"double\"" +
                "  } ]" +
                "}";
    }

    private static byte[] AvroSerialize(GenericData.Record Event){
        Injection<GenericData.Record, byte[]> recordInjection;
        recordInjection = GenericAvroCodecs.toBinary(Event.getSchema());
        return recordInjection.apply(Event);
    }
}
