import com.rabbitmq.client.ConnectionFactory;
import org.apache.avro.Schema;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class Sender {

    public static void main(String[] args){
        Jedis j = new Jedis(System.getenv("REDIS_HOST"));
        j.auth("REDIS_PASSWORD");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("RABBITMQ_HOST"));
        factory.setUsername(System.getenv("RABBITMQ_USERNAME"));
        factory.setPassword(System.getenv("RABBITMQ_PASSWORD"));

        Map<String,RabbitmqReceiver> receivers = new ConcurrentHashMap<>();
        Set<String> typesHooked = j.smembers("Registered");
        Set<String> hooks;


        while(true){
            for(String type : typesHooked){
                hooks = j.smembers(type+":WebHooks");
                if(hooks.isEmpty() && receivers.containsKey(type)){
                    receivers.remove(type);
                }
                else if(!hooks.isEmpty()){
                    if(!receivers.containsKey(type)){
                        receivers.put(type, new RabbitmqReceiver(factory, type, new Schema.Parser().parse(j.get(type + ":AvroSchema")), hooks));
                    }
                    else{
                        receivers.get(type).updateURLs(hooks);
                    }
                }
            }

            try {
                TimeUnit.MINUTES.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }



            typesHooked = j.smembers("EventTypesHooked");
        }


    }
}
