# Event-sender

Event-sender is a java based microsservice.
It works togheter with cep-worker to call web hooks when events are detected.


Docker Setup 
------------

* Install Docker 
* Install docker-compose 
* Run on terminal:
  * ```$ script/setup```
  * ```$ script/development start``` # start the container
  * ```$ script/development stop```  # stop the container

Visit the wiki in order to learn how to interact with it


# It should only be instantiated with the sripts after the rabbitmq and redis instances of cep-worker are already up