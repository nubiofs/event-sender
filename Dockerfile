# Use official Maven's alpine image as base
FROM maven:3.6-jdk-12-alpine


#Environment Variables
ENV RABBITMQ_HOST rabbitmq-cep
ENV EXCHANGE EXCHANGE
ENV RABBITMQ_USERNAME caller1
ENV RABBITMQ_PASSWORD 12345
ENV REDIS_HOST redis-cep
ENV REDIS_PASSWORD 12345



#Compilation
RUN mkdir -p /event-sender/
ADD . /event-sender/
WORKDIR /event-sender/
RUN mvn package



CMD  java -jar target/event-sender-1.0.jar
